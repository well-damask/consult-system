/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726 (5.7.26)
 Source Host           : localhost:3306
 Source Schema         : consult_db

 Target Server Type    : MySQL
 Target Server Version : 50726 (5.7.26)
 File Encoding         : 65001

 Date: 14/07/2023 16:07:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assign_log
-- ----------------------------
DROP TABLE IF EXISTS `assign_log`;
CREATE TABLE `assign_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `counselor_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'FK，受让方（接过这个客户的咨询老师的帐号）',
  `cust_id` int(11) NOT NULL COMMENT 'FK，被分配的客户ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（分配时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（完成此次分配的主管的帐号）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  INDEX `counselor_username`(`counselor_username`) USING BTREE,
  CONSTRAINT `assign_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assign_log_ibfk_2` FOREIGN KEY (`counselor_username`) REFERENCES `user` (`username`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户分配日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assign_log
-- ----------------------------

-- ----------------------------
-- Table structure for cust
-- ----------------------------
DROP TABLE IF EXISTS `cust`;
CREATE TABLE `cust`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户的姓',
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户的名',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '手机号码',
  `wx` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '微信号',
  `gender` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '性别：男/女',
  `edu_id` int(11) NULL DEFAULT NULL COMMENT 'FK，学历ID',
  `university` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '学校',
  `major` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '专业',
  `lang_level_id` int(11) NULL DEFAULT NULL COMMENT 'FK，语言等级ID',
  `project_category_id` int(11) NULL DEFAULT NULL COMMENT 'FK，项目类别ID',
  `source_id` int(11) NULL DEFAULT NULL COMMENT 'FK，客户来源ID',
  `status_id` int(11) NULL DEFAULT NULL COMMENT 'FK，客户状态ID',
  `counselor_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'FK，负责人（咨询老师）的帐号',
  `manual_transferred` tinyint(1) NULL DEFAULT 0 COMMENT '是否是被某咨询老师转让出来的客户：0=不是，1=是',
  `auto_transferred` tinyint(1) NULL DEFAULT 0 COMMENT '是否因超过6个月未联系被系统自动转让出来：0=不是，1=是',
  `deal_date` date NULL DEFAULT NULL COMMENT '成交日期（报名日期）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（此客户信息的报备时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE,
  UNIQUE INDEX `wx`(`wx`) USING BTREE,
  INDEX `edu_id`(`edu_id`) USING BTREE,
  INDEX `lang_level_id`(`lang_level_id`) USING BTREE,
  INDEX `project_category_id`(`project_category_id`) USING BTREE,
  INDEX `source_id`(`source_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  INDEX `counselor_username`(`counselor_username`) USING BTREE,
  CONSTRAINT `cust_ibfk_1` FOREIGN KEY (`edu_id`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_2` FOREIGN KEY (`lang_level_id`) REFERENCES `lang_level` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_3` FOREIGN KEY (`project_category_id`) REFERENCES `project_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_4` FOREIGN KEY (`source_id`) REFERENCES `cust_source` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_5` FOREIGN KEY (`status_id`) REFERENCES `cust_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_6` FOREIGN KEY (`counselor_username`) REFERENCES `user` (`username`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust
-- ----------------------------

-- ----------------------------
-- Table structure for cust_backup
-- ----------------------------
DROP TABLE IF EXISTS `cust_backup`;
CREATE TABLE `cust_backup`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `backup_time` datetime NOT NULL COMMENT '备份时间',
  `cust_id` int(11) NOT NULL COMMENT '客户ID',
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户的姓',
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户的名',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '手机号码',
  `wx` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '微信号',
  `gender` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '性别：男/女',
  `edu_id` int(11) NULL DEFAULT NULL COMMENT 'FK，学历ID',
  `university` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '学校',
  `major` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '专业',
  `lang_level_id` int(11) NULL DEFAULT NULL COMMENT 'FK，语言等级ID',
  `project_category_id` int(11) NULL DEFAULT NULL COMMENT 'FK，项目类别ID',
  `source_id` int(11) NULL DEFAULT NULL COMMENT 'FK，客户来源ID',
  `status_id` int(11) NULL DEFAULT NULL COMMENT 'FK，客户状态ID',
  `counselor_user_id` int(11) NULL DEFAULT NULL COMMENT 'FK，负责人（咨询老师）的用户ID',
  `manual_transferred` tinyint(1) NULL DEFAULT 0 COMMENT '是否是被某咨询老师转让出来的客户：0=不是，1=是',
  `auto_transferred` tinyint(1) NULL DEFAULT 0 COMMENT '是否因超过6个月未联系被系统自动转让出来：0=不是，1=是',
  `deal_date` date NULL DEFAULT NULL COMMENT '成交日期（报名日期）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（此客户信息的报备时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE,
  UNIQUE INDEX `wx`(`wx`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户基本信息备份表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust_backup
-- ----------------------------

-- ----------------------------
-- Table structure for cust_info_update_log
-- ----------------------------
DROP TABLE IF EXISTS `cust_info_update_log`;
CREATE TABLE `cust_info_update_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `cust_id` int(11) NOT NULL COMMENT 'FK，基本信息被变更的客户ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（变更时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（变更人）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  CONSTRAINT `cust_info_update_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户基本信息（姓名、手机号码、微信号等）变更日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust_info_update_log
-- ----------------------------

-- ----------------------------
-- Table structure for cust_source
-- ----------------------------
DROP TABLE IF EXISTS `cust_source`;
CREATE TABLE `cust_source`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `source_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '客户来源名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户来源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust_source
-- ----------------------------

-- ----------------------------
-- Table structure for cust_status
-- ----------------------------
DROP TABLE IF EXISTS `cust_status`;
CREATE TABLE `cust_status`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `status_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '状态名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust_status
-- ----------------------------

-- ----------------------------
-- Table structure for cust_status_update_log
-- ----------------------------
DROP TABLE IF EXISTS `cust_status_update_log`;
CREATE TABLE `cust_status_update_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `cust_id` int(11) NOT NULL COMMENT 'FK，状态被变更的客户ID',
  `old_status_id` int(11) NULL DEFAULT NULL COMMENT '变更前的状态',
  `new_status_id` int(11) NULL DEFAULT NULL COMMENT '变更后的状态',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（变更时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（变更人）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  INDEX `old_status_id`(`old_status_id`) USING BTREE,
  INDEX `new_status_id`(`new_status_id`) USING BTREE,
  CONSTRAINT `cust_status_update_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_status_update_log_ibfk_2` FOREIGN KEY (`old_status_id`) REFERENCES `cust_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_status_update_log_ibfk_3` FOREIGN KEY (`new_status_id`) REFERENCES `cust_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户状态变更日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust_status_update_log
-- ----------------------------

-- ----------------------------
-- Table structure for data_export_log
-- ----------------------------
DROP TABLE IF EXISTS `data_export_log`;
CREATE TABLE `data_export_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `excel_file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '被导出的excel文件名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（导出时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（导出人）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '数据导出日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_export_log
-- ----------------------------

-- ----------------------------
-- Table structure for education
-- ----------------------------
DROP TABLE IF EXISTS `education`;
CREATE TABLE `education`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `edu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '学历',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '学历表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of education
-- ----------------------------

-- ----------------------------
-- Table structure for followup_log
-- ----------------------------
DROP TABLE IF EXISTS `followup_log`;
CREATE TABLE `followup_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `cust_id` int(11) NOT NULL COMMENT 'FK，被跟进的客户ID',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '跟进情况',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（跟进时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（完成此次跟进的咨询老师的帐号）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  CONSTRAINT `followup_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '跟进日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of followup_log
-- ----------------------------

-- ----------------------------
-- Table structure for lang_level
-- ----------------------------
DROP TABLE IF EXISTS `lang_level`;
CREATE TABLE `lang_level`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `level_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '语言等级',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '语言等级表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lang_level
-- ----------------------------

-- ----------------------------
-- Table structure for login_log
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `login_time` datetime NOT NULL COMMENT '登录时间',
  `login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '登录IP',
  `login_location` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '登录地区',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '登录者帐号',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE,
  CONSTRAINT `login_log_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '登录日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login_log
-- ----------------------------

-- ----------------------------
-- Table structure for payment_type
-- ----------------------------
DROP TABLE IF EXISTS `payment_type`;
CREATE TABLE `payment_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '付款方式',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '付款方式表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment_type
-- ----------------------------

-- ----------------------------
-- Table structure for project_category
-- ----------------------------
DROP TABLE IF EXISTS `project_category`;
CREATE TABLE `project_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '项目类别名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '项目类别表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of project_category
-- ----------------------------

-- ----------------------------
-- Table structure for transfer_log
-- ----------------------------
DROP TABLE IF EXISTS `transfer_log`;
CREATE TABLE `transfer_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `cust_id` int(11) NOT NULL COMMENT 'FK，被转让的客户ID',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '转让说明',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（转让时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（转让方，之前负责这个客户的咨询老师的帐号，如果是超过6个月没有联系被系统自动转让的情况，创建人是SYSTEM））',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  CONSTRAINT `transfer_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户转让日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transfer_log
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '帐号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `real_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '真实姓名',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '帐号状态：1=启用，0=禁用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户信息表（用户包括：咨询老师、咨询主管、管理员）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
