package com.example.consultapi.controller;

import com.example.consultapi.domain.AssignLog;
import com.example.consultapi.service.AssignLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/assignlog")
public class AssignLogController extends BaseController<AssignLog, AssignLogService> {
    @Resource
    AssignLogService assignLogService;

    @GetMapping("/list")
    public PageInfo<Map> getAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Map> list = this.assignLogService.getAll();
        return PageInfo.of(list);
    }
}
