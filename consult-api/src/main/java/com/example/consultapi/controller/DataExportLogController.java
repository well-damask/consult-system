package com.example.consultapi.controller;

import com.example.consultapi.domain.DataExportLog;
import com.example.consultapi.service.DataExportLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/dataexportlog")
public class DataExportLogController {
    @Resource
    DataExportLogService dataExportLogService;

    @GetMapping("/list")
    public PageInfo<DataExportLog> getAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<DataExportLog> list = this.dataExportLogService.getAll();
        return PageInfo.of(list);
    }
}
