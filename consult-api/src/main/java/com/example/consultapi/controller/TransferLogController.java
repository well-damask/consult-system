package com.example.consultapi.controller;

import com.example.consultapi.domain.TransferLog;
import com.example.consultapi.service.TransferLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/transfer")
public class TransferLogController extends BaseController<TransferLog, TransferLogService> {
    @Resource
    TransferLogService transferLogService;

    @PostMapping("/allocation")
    public boolean allocation(@RequestBody Model model) {
        return this.transferLogService.allocation(model);
    }

    @GetMapping("/list")
    public PageInfo<Map> getAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Map> list = this.transferLogService.getAll();
        return PageInfo.of(list);
    }

    public class Model {
        private List<Map> cust;
        private List<Map> user;
    }
}
