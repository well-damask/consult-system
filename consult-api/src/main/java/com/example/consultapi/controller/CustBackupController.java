package com.example.consultapi.controller;

import com.example.consultapi.domain.CustBackup;
import com.example.consultapi.service.CustBackupService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/backup")
public class CustBackupController extends BaseController<CustBackup, CustBackupService> {

}
