package com.example.consultapi.controller;

import com.example.consultapi.domain.PaymentType;
import com.example.consultapi.service.PaymentTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/payment")
public class PaymentTypeController extends BaseController<PaymentType, PaymentTypeService> {
    @Resource
    PaymentTypeService paymentTypeService;

    @GetMapping()
    public List<PaymentType> getPayType() {
        return this.paymentTypeService.getPayType();
    }
}
