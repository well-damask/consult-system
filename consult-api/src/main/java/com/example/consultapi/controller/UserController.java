package com.example.consultapi.controller;

import com.example.consultapi.domain.User;
import com.example.consultapi.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController<User, UserService> {
    @Resource
    UserService userService;

    @GetMapping("/identity")
    public List<String> getUserIdentity(String username) {
        return this.userService.getUserIdentity(username);
    }

    @GetMapping("/userinfo")
    public List<Map> getUserInfo() {
        return this.userService.getUserInfo();
    }

    @GetMapping("/list")
    public List<Map> getAll() {
        return this.userService.getAll();
    }

    //根据用户ID查询用户角色信息
    @GetMapping("/role")
    public List<Map<String, Object>> getUserRoleById(int userId) {
        return this.userService.getUserRoleById(userId);
    }
}

