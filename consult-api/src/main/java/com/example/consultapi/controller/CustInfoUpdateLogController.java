package com.example.consultapi.controller;

import com.example.consultapi.domain.CustInfoUpdateLog;
import com.example.consultapi.service.CustInfoUpdateLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/custinfoupdatelog")
public class CustInfoUpdateLogController extends BaseController<CustInfoUpdateLog, CustInfoUpdateLogService> {
    @Resource
    CustInfoUpdateLogService custInfoUpdateLogService;

    @GetMapping("/list")
    public PageInfo<Map> getAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Map> list = this.custInfoUpdateLogService.getAll();
        return PageInfo.of(list);
    }

}
