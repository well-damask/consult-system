package com.example.consultapi.controller;

import com.example.consultapi.domain.LoginLog;
import com.example.consultapi.service.LoginLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/loginlog")
public class LoginLogController extends BaseController<LoginLog, LoginLogService> {
    @Resource
    LoginLogService loginLogService;

    @GetMapping("/list")
    public PageInfo<Map> getAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Map> list = this.loginLogService.getAll();
        return PageInfo.of(list);
    }

}
