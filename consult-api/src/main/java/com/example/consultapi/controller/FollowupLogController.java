package com.example.consultapi.controller;

import com.example.consultapi.domain.FollowupLog;
import com.example.consultapi.service.FollowupLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/followup")
public class FollowupLogController extends BaseController<FollowupLog, FollowupLogService> {
    @Resource
    FollowupLogService followupLogService;

    @GetMapping("/custid")
    public List<FollowupLog> getListByCustId(int custId) {
        return this.followupLogService.getListByCustId(custId);
    }

    @GetMapping("/list")
    public PageInfo<Map> getAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Map> list = this.followupLogService.getAll();
        return PageInfo.of(list);
    }
}
