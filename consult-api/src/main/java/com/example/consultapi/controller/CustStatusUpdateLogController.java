package com.example.consultapi.controller;

import com.example.consultapi.domain.CustStatusUpdateLog;
import com.example.consultapi.service.CustStatusUpdateLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/custstatusupdatelog")
public class CustStatusUpdateLogController extends BaseController<CustStatusUpdateLog, CustStatusUpdateLogService> {
    @Resource
    CustStatusUpdateLogService custStatusUpdateLogService;

    @GetMapping("/list")
    public PageInfo<Map> getAll(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Map> list = this.custStatusUpdateLogService.getAll();
        return PageInfo.of(list);
    }

}
