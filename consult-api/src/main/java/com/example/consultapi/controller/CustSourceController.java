package com.example.consultapi.controller;

import com.example.consultapi.domain.CustSource;
import com.example.consultapi.service.CustSourceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/source")
public class CustSourceController extends BaseController<CustSource, CustSourceService> {
    @Resource
    CustSourceService custSourceService;

    @GetMapping()
    public List<CustSource> getCustSource() {
        return this.custSourceService.getCustSource();
    }
}
