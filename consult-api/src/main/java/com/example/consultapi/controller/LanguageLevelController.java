package com.example.consultapi.controller;

import com.example.consultapi.domain.LangLevel;
import com.example.consultapi.service.LangLevelService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/level")
public class LanguageLevelController extends BaseController<LangLevel, LangLevelService> {
    @Resource
    LangLevelService langLevelService;

    @GetMapping()
    public List<LangLevel> getLevel() {
        return this.langLevelService.getLevel();
    }

}
