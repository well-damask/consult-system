package com.example.consultapi.controller;


import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.example.consultapi.domain.Cust;
import com.example.consultapi.dto.AssignModel;
import com.example.consultapi.mapper.UserMapper;
import com.example.consultapi.service.CustService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cust")
public class CustController extends BaseController<Cust, CustService> {
    @Resource
    CustService custService;
    @Resource
    UserMapper userMapper;

    @GetMapping("/dupcheck")
    public List<Cust> getlistByDupCheck(CustModel entity) {
        return this.custService.getlistByDupCheck(entity);
    }

    @PostMapping("/list")
    public PageInfo<Map> getListByPhoneOrName(@RequestBody CustModel entity) {
        // 获取当前用户身份
        List<String> identityList = this.userMapper.getUserIdentity(entity.getCounselorUsername());
        entity.setUserIdentity(identityList);
        //分页查询
        System.out.println(entity.getPageSize());
        PageHelper.startPage(entity.getPageNo(), entity.getPageSize());
        List<Map> list = this.custService.getListByPhoneOrName(entity);
        return PageInfo.of(list);
    }

    @GetMapping("/export")
    public void getListByPhoneOrName(CustModel entity, HttpServletResponse response) throws IOException {
        // 获取当前用户身份
        List<String> identityList = this.userMapper.getUserIdentity(entity.getCounselorUsername());
        entity.setUserIdentity(identityList);
        //分页查询
        List<Map> list = this.custService.getListByPhoneOrName(entity);
        ExcelWriter writer = ExcelUtil.getWriter();
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = list.get(i);
            map.put("序号", i + 1);
        }
        writer.addHeaderAlias("序号", "序号");
        writer.addHeaderAlias("id", "数据库中的编号");
        writer.addHeaderAlias("name", "姓名");
        writer.addHeaderAlias("phone", "手机号");
        writer.addHeaderAlias("wx", "微信号");
        writer.addHeaderAlias("gender", "性别");
        writer.addHeaderAlias("statusName", "状态");
        writer.addHeaderAlias("counselorName", "负责人");
        writer.addHeaderAlias("createTime", "备份日期");
        writer.addHeaderAlias("lastFollowupTime", "上一次跟进日期");
        writer.addHeaderAlias("eduName", "学历");
        writer.addHeaderAlias("university", "学校");
        writer.addHeaderAlias("major", "专业");
        writer.addHeaderAlias("languageName", "语言等级");
        writer.addHeaderAlias("categoryName", "项目类别");
        writer.addHeaderAlias("sourceName", "来源");
        writer.addHeaderAlias("manualTransferred", "手动转让");
        writer.addHeaderAlias("autoTransferred", "自动转让");
        writer.addHeaderAlias("dealDate", "成交日期");
        writer.addHeaderAlias("createBy", "创建者");
        writer.addHeaderAlias("updateTime", "最近更新日期");
        writer.addHeaderAlias("updateBy", "最近更新人");
        writer.write(list, true);
        //response为HttpServletResponse对象
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        //test.xls是弹出下载对话框的文件名，不能为中文，中文请自行编码
        response.setHeader("Content-Disposition", "attachment;filename=test.xls");
        ServletOutputStream out = response.getOutputStream();
        writer.flush(out, true);
        // 关闭writer，释放内存
        writer.close();
        //此处记得关闭输出Servlet流
        IoUtil.close(out);
    }

    //查询待分配客户
    @GetMapping("/transfered")
    public PageInfo<Map> getPublicCust(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Map> list = this.custService.PublicCust();
        return PageInfo.of(list);
    }

    //分配客户
    @PutMapping("/assign")
    public boolean assign(@RequestBody AssignModel assignModel) {
        return this.custService.assign(assignModel);
    }

    @Data
    public static class CustModel {
        private String name;
        private String phone;
        private String wx;
        private String counselorUsername;
        private int pageNo;
        private int pageSize;
        private List<String> userIdentity;

    }
}

