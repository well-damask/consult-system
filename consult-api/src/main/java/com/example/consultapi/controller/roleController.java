package com.example.consultapi.controller;

import com.example.consultapi.domain.Role;
import com.example.consultapi.service.RoleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/role")
public class roleController extends BaseController<Role, RoleService> {
    @Resource
    RoleService roleService;

    @GetMapping("/list")
    public List<Role> getAll() {
        return this.roleService.getAll();
    }
}
