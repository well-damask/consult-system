package com.example.consultapi.controller;

import com.example.consultapi.domain.User;
import com.example.consultapi.service.UserService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@RestController
public class LoginController {
    @Autowired
    UserService userService;

    @PostMapping("/login")
    public User login(@Valid @RequestBody LoginUserModel entity) {
        return this.userService.login(entity.username, entity.password);
    }

    @Data
    private static class LoginUserModel {
        @NotNull(message = "请输入帐号")
        @Pattern(regexp = "^\\w{4,20}$", message = "帐号由由字母、数字、下划线组成，长度为4-20位")
        private String username;
        @NotNull(message = "请输入密码")
        private String password;
    }
}