package com.example.consultapi.controller;

import com.example.consultapi.domain.UserRole;
import com.example.consultapi.dto.UserRoleUpdateModel;
import com.example.consultapi.service.UserRoleService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/userrole")
public class UserRoleController extends BaseController<UserRole, UserRoleService> {
    @Resource
    UserRoleService userRoleService;

    @PutMapping("/update")
    public boolean updateUserRole(@RequestBody UserRoleUpdateModel model) {
        return this.userRoleService.updateUserRole(model);
    }

}
