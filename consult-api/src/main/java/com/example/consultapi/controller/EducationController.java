package com.example.consultapi.controller;

import com.example.consultapi.domain.Education;
import com.example.consultapi.service.EducationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/edu")
public class EducationController extends BaseController<Education, EducationService> {
    @Resource
    EducationService educationService;

    @GetMapping()
    public List<Education> getEducation() {
        return this.educationService.getEducation();
    }
}
