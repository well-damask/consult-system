package com.example.consultapi.controller;

import com.example.consultapi.domain.ProjectCategory;
import com.example.consultapi.service.ProjectCategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/pro")
public class ProjectCategoryController extends BaseController<ProjectCategory, ProjectCategoryService> {
    @Resource
    ProjectCategoryService projectCategoryService;

    @GetMapping()
    public List<ProjectCategory> getProject() {
        return this.projectCategoryService.getProject();
    }
}
