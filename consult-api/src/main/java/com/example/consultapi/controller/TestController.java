package com.example.consultapi.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class TestController {
    @Resource
    ObjectMapper OM;

    // GET http://localhost:8080/test1?statusNames[]=AA&statusNames[]=BB
    @PostMapping("/test1")
    public String test1(@RequestBody QueryModel model) {
        try {
            System.out.println(OM.writeValueAsString(model));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return "OK";
    }

    @Data
    static class QueryModel {
        private String fullName;
        private String phone;
        private List<String> statusNames;
    }

}
