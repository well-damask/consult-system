package com.example.consultapi.controller;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 通用Controller父类
 *
 * @param <T>        实体类型，例如：User
 * @param <TService> 实体类型对应的业务接口类型，例如UserService
 */
@Validated
// TService extends IService<T> 表示：TService必须是一个继承了IService接口的业务接口。
public abstract class BaseController<T, TService extends IService<T>> {

    @Autowired
    protected TService service;

    @GetMapping("/{id}")
    public T getById(@PathVariable int id) {
        return this.service.getById(id);
    }

    @DeleteMapping("/{id}")
    public boolean removeById(@PathVariable @Validated @NotNull int id) {
        return this.service.removeById(id);
    }

    @DeleteMapping("/batchDelete")
    public boolean removeBatchByIds(@RequestBody List<Integer> ids) {
        return this.service.removeBatchByIds(ids);
    }

    @PutMapping
    public boolean updateById(@Valid @RequestBody T entity) {
        return this.service.updateById(entity);
    }

    @PostMapping
    public boolean save(@Valid @RequestBody T entity) {
        return this.service.save(entity);
    }

}
