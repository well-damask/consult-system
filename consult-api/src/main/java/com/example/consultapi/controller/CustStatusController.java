package com.example.consultapi.controller;

import com.example.consultapi.domain.CustStatus;
import com.example.consultapi.service.CustStatusService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/status")
public class CustStatusController extends BaseController<CustStatus, CustStatusService> {
    @Resource
    CustStatusService custStatusService;

    @GetMapping()
    public List<CustStatus> getCustStatus() {
        return this.custStatusService.getCustStatus();
    }
}
