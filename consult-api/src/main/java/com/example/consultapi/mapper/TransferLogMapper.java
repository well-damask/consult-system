package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.TransferLog;

import java.util.List;
import java.util.Map;


/**
 * @author helan
 * @description 针对表【transfer_log(客户转让日志表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.TransferLog
 */
public interface TransferLogMapper extends BaseMapper<TransferLog> {

    /**
     * 查询所有转让记录
     *
     * @return 转让记录的集合
     */
    List<Map> getAll();
}




