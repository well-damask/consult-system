package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.CustStatus;

/**
 * @author helan
 * @description 针对表【cust_status(客户状态表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.CustStatus
 */
public interface CustStatusMapper extends BaseMapper<CustStatus> {

}




