package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.CustSource;

/**
 * @author helan
 * @description 针对表【cust_source(客户来源表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.CustSource
 */
public interface CustSourceMapper extends BaseMapper<CustSource> {

}




