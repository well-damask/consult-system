package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.controller.CustController;
import com.example.consultapi.domain.Cust;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【cust(客户表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:25
 * @Entity com.example.consultapi.domain.Cust
 */
public interface CustMapper extends BaseMapper<Cust> {

    /**
     * 根据客户电话号码查询客户信息
     *
     * @param phone 客户电话号码
     * @return 客户个数
     */
    int getCustByPhone(String phone);

    /**
     * 根据微信号查询客户信息
     *
     * @param weChat 微信号
     * @return 客户个数
     */
    int getCustByWeChat(String weChat);

    /**
     * 根据用户输入的条件查重
     *
     * @param entity 条件实体
     * @return 客户实体集合
     */
    List<Cust> getlistByDupCheck(CustController.CustModel entity);

    /**
     * 根据电话或号码查询客户信息
     *
     * @param entity 查询条件实体
     * @return 客户实体集合
     */
    List<Map> getListByPhoneOrName(CustController.CustModel entity);

    List<Map> PublicCust();
    /**
     * 查询超过六个月未联系的客户id
     * @return 返回跟进日志对象的集合
     */
    List<Map<String,Object>> getCustIdBySixMonthNoConnect();
}




