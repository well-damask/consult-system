package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.PaymentType;


/**
 * @author helan
 * @description 针对表【payment_type(付款方式表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.PaymentType
 */
public interface PaymentTypeMapper extends BaseMapper<PaymentType> {

}




