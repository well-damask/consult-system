package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.CustBackup;


/**
 * @author helan
 * @description 针对表【cust_backup(客户基本信息备份表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.CustBackup
 */
public interface CustBackupMapper extends BaseMapper<CustBackup> {
}




