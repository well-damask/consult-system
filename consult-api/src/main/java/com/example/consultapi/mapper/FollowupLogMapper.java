package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.FollowupLog;

import java.util.List;
import java.util.Map;


/**
 * @author helan
 * @description 针对表【followup_log(跟进日志表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.FollowupLog
 */
public interface FollowupLogMapper extends BaseMapper<FollowupLog> {
    /**
     * 查询跟进日志所有记录
     *
     * @return 跟进记录的集合
     */
    List<Map> getAll();


}




