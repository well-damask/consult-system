package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.User;

import java.util.List;
import java.util.Map;


/**
 * @author helan
 * @description 针对表【user(用户信息表（用户包括：咨询老师、咨询主管、管理员）)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.User
 */
public interface UserMapper extends BaseMapper<User> {
    /**
     * 根据用户名查询用户身份
     *
     * @param username 用户名
     * @return 用户身份的string集合
     */
    List<String> getUserIdentity(String username);

    /**
     * 获取用户的信息（真实姓名，成交率，客户总数）
     *
     * @return 返回客户信息集合
     */
    List<Map> getUserInfo();

    /**
     * 查询用户所有信息
     *
     * @return 用户信息列表
     */
    List<Map> getAll();

    List<Map<String, Object>> getUserRoleById(int userId);


}




