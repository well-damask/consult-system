package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.LangLevel;


/**
 * @author helan
 * @description 针对表【lang_level(语言等级表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.LangLevel
 */
public interface LangLevelMapper extends BaseMapper<LangLevel> {

}




