package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.Education;

/**
 * @author helan
 * @description 针对表【education(学历表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.Education
 */
public interface EducationMapper extends BaseMapper<Education> {

}




