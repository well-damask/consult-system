package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.Role;

/**
 * @author 19223
 * @description 针对表【role(角色表)】的数据库操作Mapper
 * @createDate 2023-07-31 14:14:12
 * @Entity com/example/consultapi.domain.Role
 */
public interface RoleMapper extends BaseMapper<Role> {

}




