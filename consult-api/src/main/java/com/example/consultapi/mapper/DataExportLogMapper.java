package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.DataExportLog;


/**
 * @author helan
 * @description 针对表【data_export_log(数据导出日志表)】的数据库操作Mapper
 * @createDate 2023-07-17 15:58:26
 * @Entity com.example.consultapi.domain.DataExportLog
 */
public interface DataExportLogMapper extends BaseMapper<DataExportLog> {

}




