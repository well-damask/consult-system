package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.controller.TransferLogController;
import com.example.consultapi.domain.*;
import com.example.consultapi.mapper.CustMapper;
import com.example.consultapi.mapper.TransferLogMapper;
import com.example.consultapi.mapper.UserMapper;
import com.example.consultapi.service.CustBackupService;
import com.example.consultapi.service.CustService;
import com.example.consultapi.service.TransferLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【transfer_log(客户转让日志表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class TransferLogServiceImpl extends ServiceImpl<TransferLogMapper, TransferLog> implements TransferLogService {
    @Resource
    CustMapper custMapper;
    @Resource
    CustService custService;
    @Resource
    UserMapper userMapper;
    @Resource
    CustBackupService custBackupService;
    @Resource
    TransferLogMapper transferLogMapper;

    @Override
    @Transactional
    public boolean save(TransferLog entity) {
        //拿到客户原信息
        Cust beforeBackupCust = this.custMapper.selectById(entity.getCustId());
        CustBackup custBackup = new CustBackup();
        //给备份实体赋值
        custBackup.setCustId(beforeBackupCust.getId());
        custBackup.setBackupTime(LocalDateTime.now());
        custBackup.setLastName(beforeBackupCust.getLastName());
        custBackup.setFirstName(beforeBackupCust.getFirstName());
        custBackup.setPhone(beforeBackupCust.getPhone());
        custBackup.setWx(beforeBackupCust.getWx());
        custBackup.setGender(beforeBackupCust.getGender());
        custBackup.setEduId(beforeBackupCust.getEduId());
        custBackup.setUniversity(beforeBackupCust.getUniversity());
        custBackup.setMajor(beforeBackupCust.getMajor());
        custBackup.setLangLevelId(beforeBackupCust.getLangLevelId());
        custBackup.setProjectCategoryId(beforeBackupCust.getProjectCategoryId());
        custBackup.setSourceId(beforeBackupCust.getSourceId());
        custBackup.setStatusId(beforeBackupCust.getStatusId());
        if (beforeBackupCust.getCounselorUsername() == null) throw new NullPointerException("该客户已经被转让");
        //负责人id(通过用户负责人的username查询)
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.eq("username", beforeBackupCust.getCounselorUsername());
        User user = this.userMapper.selectOne(queryWrapper);
        custBackup.setCounselorUserId(user.getId());
        custBackup.setManualTransferred(beforeBackupCust.getManualTransferred());
        custBackup.setAutoTransferred(beforeBackupCust.getAutoTransferred());
        custBackup.setDealDate(beforeBackupCust.getDealDate());
        //备份
        this.custBackupService.save(custBackup);
        //修改客户表中该客户的信息
        this.custService.lambdaUpdate().set(Cust::getCounselorUsername, null)
                .set(Cust::getManualTransferred, 1)
                .eq(BaseEntity::getId, entity.getCustId())
                .update();
        //插入转让记录到表中
        return super.save(entity);
    }

    @Override
    public boolean allocation(TransferLogController.Model model) {

        return false;
    }

    @Override
    public List<Map> getAll() {
        return this.transferLogMapper.getAll();
    }
}




