package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.CustBackup;

/**
 * @author helan
 * @description 针对表【cust_backup(客户基本信息备份表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface CustBackupService extends IService<CustBackup> {

}
