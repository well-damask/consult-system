package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.controller.CustController;
import com.example.consultapi.domain.Cust;
import com.example.consultapi.dto.AssignModel;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【cust(客户表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface CustService extends IService<Cust> {
    /**
     * 通过用户输入的查重条件进行查重
     *
     * @param entity 用户传入的需要查重客户
     * @return 返回一个客户集合
     */
    List<Cust> getlistByDupCheck(CustController.CustModel entity);

    /**
     * 客户信息查询
     *
     * @param entity 查询条件
     * @return map类型的list集合
     */
    List<Map> getListByPhoneOrName(CustController.CustModel entity);

    /**
     * 查询没有负责人的客户
     *
     * @return 返回客户列表
     */
    List<Map> PublicCust();

    /**
     * 客户分配
     *
     * @param assignModel （带有客户的id，负责人账号，以及咨询老师账号）
     * @return
     */
    boolean assign(AssignModel assignModel);
}
