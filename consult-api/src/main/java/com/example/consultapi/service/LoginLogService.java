package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.LoginLog;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【login_log(登录日志表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface LoginLogService extends IService<LoginLog> {
    /**
     * 查询登录日志表中的所有记录
     *
     * @return 所有用户的登录记录集合
     */
    List<Map> getAll();
}
