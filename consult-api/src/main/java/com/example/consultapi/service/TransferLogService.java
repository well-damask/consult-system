package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.controller.TransferLogController;
import com.example.consultapi.domain.TransferLog;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【transfer_log(客户转让日志表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface TransferLogService extends IService<TransferLog> {
    /**
     * 分配客户
     *
     * @param model 参与分配的客户和咨询老师
     * @return
     */
    boolean allocation(TransferLogController.Model model);

    /**
     * 查询所有转让记录
     *
     * @return 转让记录的集合
     */
    List<Map> getAll();
}
