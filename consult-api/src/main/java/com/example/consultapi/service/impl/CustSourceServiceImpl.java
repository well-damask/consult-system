package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.CustSource;
import com.example.consultapi.mapper.CustSourceMapper;
import com.example.consultapi.service.CustSourceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author helan
 * @description 针对表【cust_source(客户来源表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class CustSourceServiceImpl extends ServiceImpl<CustSourceMapper, CustSource>
        implements CustSourceService {
    @Resource
    CustSourceMapper custSourceMapper;

    @Override
    public List<CustSource> getCustSource() {
        return this.custSourceMapper.selectList(new QueryWrapper<>());
    }
}



