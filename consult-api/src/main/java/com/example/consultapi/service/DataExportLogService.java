package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.DataExportLog;

import java.util.List;

/**
 * @author helan
 * @description 针对表【data_export_log(数据导出日志表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface DataExportLogService extends IService<DataExportLog> {
    /**
     * 查询所有导出记录
     *
     * @return 导出记录的集合
     */
    List<DataExportLog> getAll();
}
