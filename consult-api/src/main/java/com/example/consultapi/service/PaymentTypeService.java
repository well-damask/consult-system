package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.PaymentType;

import java.util.List;

/**
 * @author helan
 * @description 针对表【payment_type(付款方式表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface PaymentTypeService extends IService<PaymentType> {
    /**
     * 获取付款方式表中的信息
     *
     * @return 返回付款方式集合
     */
    List<PaymentType> getPayType();
}
