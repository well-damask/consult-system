package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.LangLevel;

import java.util.List;

/**
 * @author helan
 * @description 针对表【lang_level(语言等级表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface LangLevelService extends IService<LangLevel> {
    /**
     * 获取语言等级表中的数据
     *
     * @return 返回语言等级集合
     */
    List<LangLevel> getLevel();
}
