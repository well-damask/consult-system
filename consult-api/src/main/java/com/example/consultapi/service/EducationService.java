package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.Education;

import java.util.List;

/**
 * @author helan
 * @description 针对表【education(学历表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface EducationService extends IService<Education> {
    /**
     * 获取学历表信息
     *
     * @return 返回学历集合
     */
    List<Education> getEducation();
}
