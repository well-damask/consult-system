package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.Role;

import java.util.List;

/**
 * @author 19223
 * @description 针对表【role(角色表)】的数据库操作Service
 * @createDate 2023-07-31 14:14:12
 */
public interface RoleService extends IService<Role> {
    /**
     * 查询所有的用户角色
     *
     * @return 用户角色集合
     */
    List<Role> getAll();
}
