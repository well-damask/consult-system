package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.Education;
import com.example.consultapi.mapper.EducationMapper;
import com.example.consultapi.service.EducationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author helan
 * @description 针对表【education(学历表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class EducationServiceImpl extends ServiceImpl<EducationMapper, Education>
        implements EducationService {
    @Resource
    EducationMapper educationMapper;

    @Override
    public List<Education> getEducation() {
        return this.educationMapper.selectList(new QueryWrapper<Education>());
    }

    @Override
    public boolean save(Education entity) {
        // 补填数据，因为有的数据不适合由用户界面提供
        entity.setCreateTime(LocalDateTime.now());

        return super.save(entity);
    }
}




