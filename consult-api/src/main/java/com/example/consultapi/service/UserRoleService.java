package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.UserRole;
import com.example.consultapi.dto.UserRoleUpdateModel;

/**
 * @author 19223
 * @description 针对表【user_role(用户角色关系表)】的数据库操作Service
 * @createDate 2023-07-31 14:14:12
 */
public interface UserRoleService extends IService<UserRole> {
    /**
     * 更新用户角色
     *
     * @param model
     * @return
     */
    boolean updateUserRole(UserRoleUpdateModel model);
}
