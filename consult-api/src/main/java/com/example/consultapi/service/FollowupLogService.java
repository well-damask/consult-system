package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.FollowupLog;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【followup_log(跟进日志表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface FollowupLogService extends IService<FollowupLog> {
    /**
     * 根据客户id查询跟进记录
     *
     * @param custId 客户的id
     * @return 客户跟进记录的集合
     */
    List<FollowupLog> getListByCustId(int custId);

    /**
     * 查询跟进日志所有记录
     *
     * @return 跟进记录的集合
     */
    List<Map> getAll();
}
