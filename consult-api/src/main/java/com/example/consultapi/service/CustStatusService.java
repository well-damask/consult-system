package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.CustStatus;

import java.util.List;

/**
 * @author helan
 * @description 针对表【cust_status(客户状态表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface CustStatusService extends IService<CustStatus> {
    /**
     * 获取客户状态表中的信息
     *
     * @return 客户状态集合
     */
    List<CustStatus> getCustStatus();
}
