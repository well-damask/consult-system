package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.DataExportLog;
import com.example.consultapi.mapper.DataExportLogMapper;
import com.example.consultapi.service.DataExportLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author helan
 * @description 针对表【data_export_log(数据导出日志表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class DataExportLogServiceImpl extends ServiceImpl<DataExportLogMapper, DataExportLog>
        implements DataExportLogService {
    @Resource
    DataExportLogMapper dataExportLogMapper;

    @Override
    public List<DataExportLog> getAll() {
        return this.dataExportLogMapper.selectList(new QueryWrapper<>());
    }
}




