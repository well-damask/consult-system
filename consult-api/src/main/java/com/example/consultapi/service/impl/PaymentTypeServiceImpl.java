package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.PaymentType;
import com.example.consultapi.mapper.PaymentTypeMapper;
import com.example.consultapi.service.PaymentTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author helan
 * @description 针对表【payment_type(付款方式表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class PaymentTypeServiceImpl extends ServiceImpl<PaymentTypeMapper, PaymentType>
        implements PaymentTypeService {
    @Resource
    PaymentTypeMapper paymentTypeMapper;

    @Override
    public List<PaymentType> getPayType() {
        return this.paymentTypeMapper.selectList(new QueryWrapper<PaymentType>());
    }
}




