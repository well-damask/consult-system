package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.CustInfoUpdateLog;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【cust_info_update_log(客户基本信息（姓名、手机号码、微信号等）变更日志表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface CustInfoUpdateLogService extends IService<CustInfoUpdateLog> {
    /**
     * 查询所有客户信息修改的日志记录
     *
     * @return 日志记录集合
     */
    List<Map> getAll();
}
