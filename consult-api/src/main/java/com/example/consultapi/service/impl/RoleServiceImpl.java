package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.Role;
import com.example.consultapi.mapper.RoleMapper;
import com.example.consultapi.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 19223
 * @description 针对表【role(角色表)】的数据库操作Service实现
 * @createDate 2023-07-31 14:14:12
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role>
        implements RoleService {
    @Override
    public List<Role> getAll() {
        return this.lambdaQuery().list();

    }
}




