package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.CustSource;

import java.util.List;

/**
 * @author helan
 * @description 针对表【cust_source(客户来源表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface CustSourceService extends IService<CustSource> {
    /**
     * 获取客户来源
     *
     * @return 返回客户来源的集合
     */
    List<CustSource> getCustSource();
}
