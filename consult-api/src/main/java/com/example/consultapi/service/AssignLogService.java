package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.AssignLog;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【assign_log(客户分配日志表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:25
 */
public interface AssignLogService extends IService<AssignLog> {
    /**
     * 查询所有分配记录
     *
     * @return 分配记录集合
     */
    List<Map> getAll();
}
