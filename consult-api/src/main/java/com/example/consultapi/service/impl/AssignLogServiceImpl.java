package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.AssignLog;
import com.example.consultapi.mapper.AssignLogMapper;
import com.example.consultapi.service.AssignLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【assign_log(客户分配日志表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:25
 */
@Service
public class AssignLogServiceImpl extends ServiceImpl<AssignLogMapper, AssignLog>
        implements AssignLogService {
    @Resource
    AssignLogMapper assignLogMapper;

    @Override
    public List<Map> getAll() {
        return this.assignLogMapper.getAll();
    }
}




