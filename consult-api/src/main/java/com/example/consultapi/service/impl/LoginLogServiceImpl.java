package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.LoginLog;
import com.example.consultapi.mapper.LoginLogMapper;
import com.example.consultapi.service.LoginLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【login_log(登录日志表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLog>
        implements LoginLogService {
    @Resource
    LoginLogMapper loginLogMapper;

    @Override
    public List<Map> getAll() {
        return this.loginLogMapper.getAll();
    }
}




