package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.User;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【user(用户信息表（用户包括：咨询老师、咨询主管、管理员）)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface UserService extends IService<User> {
    /**
     * @param username 用户输入的账号
     * @param password 用户输入的密码
     * @return
     */
    User login(String username, String password);

    /**
     * 获取当前用户的身份
     *
     * @param username 当前用户名
     * @return 用户身份列表
     */
    List<String> getUserIdentity(String username);

    /**
     * 获取用户的信息（真实姓名，成交率，客户总数）
     *
     * @return 返回客户信息集合
     */
    List<Map> getUserInfo();

    /**
     * 查询用户所有信息
     *
     * @return 用户信息列表
     */
    List<Map> getAll();

    /**
     * 根据用户id查询用户角色
     *
     * @param userId 用户输入的用户id
     * @return 用户角色的集合
     */
    List<Map<String, Object>> getUserRoleById(int userId);
}
