package com.example.consultapi.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.controller.CustController;
import com.example.consultapi.domain.*;
import com.example.consultapi.dto.AssignModel;
import com.example.consultapi.mapper.*;
import com.example.consultapi.service.AssignLogService;
import com.example.consultapi.service.CustBackupService;
import com.example.consultapi.service.CustService;
import com.example.consultapi.utils.AssignUtils;
import com.example.consultapi.utils.SpringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【cust(客户表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class CustServiceImpl extends ServiceImpl<CustMapper, Cust> implements CustService {
    @Resource
    CustMapper custMapper;
    @Resource
    CustInfoUpdateLogMapper custInfoUpdateLogMapper;
    @Resource
    UserMapper userMapper;
    @Resource
    CustBackupService custBackupService;
    @Resource
    CustStatusMapper custStatusMapper;
    @Resource
    CustStatusUpdateLogMapper custStatusUpdateLogMapper;
    @Resource
    DataExportLogMapper dataExportLogMapper;
    @Resource
    AssignLogService assignLogService;

    @Override
    public boolean save(Cust entity) {
        //客户名
        String firstName = entity.getFirstName();
        //微信号
        String weChat = entity.getWx();
        //客户电话号码
        String phone = entity.getPhone();
        //数据验证
        if (StrUtil.isBlank(weChat) && StrUtil.isBlank(phone)) throw new RuntimeException("电话号码或微信必须填一个");
        //查重的结果 ,开始业务检查，处理流程
        int cust1 = 0, cust2 = 0;
        if (StrUtil.isNotBlank(phone)) cust1 = this.custMapper.getCustByPhone(entity.getPhone());
        if (StrUtil.isNotBlank(weChat)) cust2 = this.custMapper.getCustByWeChat(entity.getWx());
        //查询结果为1，抛出异常
        if (cust1 == 1 || cust2 == 1) {
            throw new RuntimeException("该客户已报备");
        }
        //查询结果为0，加入客户表
        return super.save(entity);
    }

    @Override
    public List<Cust> getlistByDupCheck(CustController.CustModel entity) {

        List<Cust> list = this.custMapper.getlistByDupCheck(entity);
        //清楚除页面显示的所有信息
        list.forEach(item -> {
            item.setAutoTransferred(null);
            item.setDealDate(null);
            item.setEduId(null);
            item.setCounselorUsername(null);
            item.setLangLevelId(null);
            item.setManualTransferred(null);
            item.setProjectCategoryId(null);
            item.setSourceId(null);
            item.setStatusId(null);
            item.setCreateBy(null);
            item.setCreateTime(null);
            item.setUpdateBy(null);
            item.setUpdateTime(null);
            item.setDelFlag(null);
        });
        return list;
    }

    @Override
    public List<Map> getListByPhoneOrName(CustController.CustModel entity) {
        return this.custMapper.getListByPhoneOrName(entity);

    }

    @Override
    @Transactional
    //完善客户信息
    public boolean updateById(Cust entity) {
        String weChat = entity.getWx().trim();
        String phone = entity.getPhone().trim();
        //数据验证（手机号码和微信号必须填一个）
        if (StrUtil.isBlank(weChat) && StrUtil.isBlank(phone)) throw new RuntimeException("电话号码或微信必须填一个");
        //业务检查(保证手机号码和微信号的唯一性)
        if (StrUtil.isNotBlank(phone)) {
            Cust cust = this.lambdaQuery().eq(Cust::getPhone, phone).one();
            if (cust != null) {
                //根据客户id判断是否为同一个客户，不是则抛出异常
                //根据更新时的电话号码查到的数据库中客户id
                int custId = cust.getId();
                //更新时的客户id
                int entityId = entity.getId();
                if (custId != entityId) throw new RuntimeException("更新后的电话号码：" + phone + "与其他客户相同");

            }
        }

        if (StrUtil.isNotBlank(weChat)) {
            Cust cust = this.lambdaQuery().eq(Cust::getWx, weChat).one();
            if (cust != null) {
                //根据客户id判断是否为同一个客户，不是则抛出异常
                //根据更新时的微信号查到的数据库中客户id
                int custId = cust.getId();
                //更新时的客户id
                int entityId = entity.getId();
                if (custId != entityId) throw new RuntimeException("更新后的微信号码：" + weChat + "与其他客户相同");
            }
        }
        //备份
        backupCust(entity.getId());
        //更新客户信息
        //查看更新后的客户状态是否为已报名
        QueryWrapper<CustStatus> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", entity.getStatusId());
        CustStatus Status = this.custStatusMapper.selectOne(queryWrapper);
        //如果已报名则设置成交日期
        if (StrUtil.equals(Status.getStatusName(), "已报名")) entity.setDealDate(LocalDate.now());
        else entity.setDealDate(null);
        //如果客户状态变化则生成状态变更日志
        //更新前的客户
        Cust cust = this.lambdaQuery().eq(Cust::getId, entity.getId()).one();
        int oldStatusId = cust.getStatusId();
        int newStatusId = entity.getStatusId();
        if (oldStatusId != newStatusId) {
            CustStatusUpdateLog custStatusUpdateLog = new CustStatusUpdateLog();
            custStatusUpdateLog.setCustId(entity.getId());
            custStatusUpdateLog.setNewStatusId(newStatusId);
            custStatusUpdateLog.setOldStatusId(oldStatusId);
            this.custStatusUpdateLogMapper.insert(custStatusUpdateLog);
        }
        //更新
        super.updateById(entity);
        //新增客户信息修改记录
        CustInfoUpdateLog log = new CustInfoUpdateLog();
        log.setCustId(entity.getId());
        this.custInfoUpdateLogMapper.insert(log);
        return true;
    }

    @Override
    @Transactional
    //单个删除
    public boolean removeById(Cust entity) {
        //备份
        backupCust(entity.getId());
        //删除
        return super.removeById(entity);
    }

    //批量删除
    @Override
    public boolean removeBatchByIds(Collection<?> list) {
        //循环备份
        for (Object id : list) {
            Cust entity = this.lambdaQuery().eq(Cust::getId, id).one();
            backupCust(entity.getId());
        }
        //批量删除
        return super.removeBatchByIds(list);
    }

    /**
     * 客户备份
     *
     * @param id 客户id
     */
    private void backupCust(int id) {
        //拿到原始客户信息
        Cust beforeBackupCust = this.custMapper.selectById(id);
        //准备备份实体
        CustBackup custBackup = new CustBackup();
        //给备份实体赋值
        custBackup.setCustId(beforeBackupCust.getId());
        custBackup.setBackupTime(LocalDateTime.now());
        custBackup.setLastName(beforeBackupCust.getLastName());
        custBackup.setFirstName(beforeBackupCust.getFirstName());
        custBackup.setPhone(beforeBackupCust.getPhone());
        custBackup.setWx(beforeBackupCust.getWx());
        custBackup.setGender(beforeBackupCust.getGender());
        custBackup.setEduId(beforeBackupCust.getEduId());
        custBackup.setUniversity(beforeBackupCust.getUniversity());
        custBackup.setMajor(beforeBackupCust.getMajor());
        custBackup.setLangLevelId(beforeBackupCust.getLangLevelId());
        custBackup.setProjectCategoryId(beforeBackupCust.getProjectCategoryId());
        custBackup.setSourceId(beforeBackupCust.getSourceId());
        custBackup.setStatusId(beforeBackupCust.getStatusId());
        //负责人id(通过用户负责人的username查询)
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.eq("username", beforeBackupCust.getCounselorUsername());
        User user = this.userMapper.selectOne(queryWrapper);
        custBackup.setCounselorUserId(user.getId());
        custBackup.setManualTransferred(beforeBackupCust.getManualTransferred());
        custBackup.setAutoTransferred(beforeBackupCust.getAutoTransferred());
        custBackup.setDealDate(beforeBackupCust.getDealDate());
        //备份
        this.custBackupService.save(custBackup);
    }

    @Override
    public List<Map> PublicCust() {
        return this.custMapper.PublicCust();
    }

    @Override
    @Transactional
    public boolean assign(AssignModel assignModel) {
        AssignUtils assignUtils = new AssignUtils(assignModel.getSelectedCusts(), assignModel.getSelectedUsernames());
        //开始分配
        Map<Integer, String> map = assignUtils.getAssignPlan();
        //批量修改客户的负责人
        List<Cust> custs = new ArrayList<>();
        map.forEach((key, value) -> {
            Cust cust = new Cust();
            cust.setCounselorUsername(value);
            cust.setId(key);
            custs.add(cust);
        });
        this.updateBatchById(custs);
        //批量生成分配日志
        List<AssignLog> assignLogs = new ArrayList<>();
        map.forEach((key, value) -> {
            AssignLog assignLog = new AssignLog();
            assignLog.setCustId(key);
            assignLog.setCounselorUsername(value);
            //设置创建人
            assignLog.setCreateBy(SpringUtils.getRequest().getHeader("Authorization"));
            assignLogs.add(assignLog);


        });
        assignLogService.saveBatch(assignLogs);
        return true;
    }
}




