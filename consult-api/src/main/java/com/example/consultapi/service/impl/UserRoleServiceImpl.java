package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.UserRole;
import com.example.consultapi.dto.UserRoleUpdateModel;
import com.example.consultapi.mapper.UserRoleMapper;
import com.example.consultapi.service.UserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 19223
 * @description 针对表【user_role(用户角色关系表)】的数据库操作Service实现
 * @createDate 2023-07-31 14:14:12
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole>
        implements UserRoleService {
    @Resource
    UserRoleMapper userRoleMapper;

    @Override
    @Transactional
    public boolean updateUserRole(UserRoleUpdateModel model) {
        //拿到用户的id和新的角色id
        int userId = model.getUserId();
        List<Integer> newRoleId = model.getNewRoleId();
        //开始更新user_role表
        //1.根据用户id删除原用户身份
        this.userRoleMapper.deleteByUserId(userId);
        //2.重新新增
        for (Integer roleId : newRoleId) {
            UserRole userRole = new UserRole();
            userRole.setRoleId(roleId);
            userRole.setUserId(userId);
            this.userRoleMapper.insert(userRole);
        }
        return true;
    }
}




