package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.CustStatusUpdateLog;
import com.example.consultapi.mapper.CustStatusUpdateLogMapper;
import com.example.consultapi.service.CustStatusUpdateLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【cust_status_update_log(客户状态变更日志表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class CustStatusUpdateLogServiceImpl extends ServiceImpl<CustStatusUpdateLogMapper, CustStatusUpdateLog>
        implements CustStatusUpdateLogService {
    @Resource
    CustStatusUpdateLogMapper custStatusUpdateLogMapper;

    @Override
    public List<Map> getAll() {
        return this.custStatusUpdateLogMapper.getAll();
    }
}




