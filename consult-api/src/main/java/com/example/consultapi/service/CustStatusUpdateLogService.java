package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.CustStatusUpdateLog;

import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【cust_status_update_log(客户状态变更日志表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface CustStatusUpdateLogService extends IService<CustStatusUpdateLog> {
    /**
     * 查询所有客户状态变更日志记录
     *
     * @return 客户状态变更日志集合
     */
    List<Map> getAll();
}
