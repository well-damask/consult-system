package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.LoginLog;
import com.example.consultapi.domain.User;
import com.example.consultapi.exception.LoginCredentialsException;
import com.example.consultapi.exception.UserDisabledException;
import com.example.consultapi.mapper.LoginLogMapper;
import com.example.consultapi.mapper.UserMapper;
import com.example.consultapi.service.UserRoleService;
import com.example.consultapi.service.UserService;
import com.example.consultapi.utils.AddressUtils;
import com.example.consultapi.utils.IpUtils;
import com.example.consultapi.utils.SpringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【user(用户信息表（用户包括：咨询老师、咨询主管、管理员）)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Resource
    UserMapper userMapper;
    @Resource
    LoginLogMapper loginLogMapper;
    @Resource
    UserRoleService roleService;

    @Override
    public User login(String username, String password) {
        //密码加密
        String hashedPassword = DigestUtils.md5DigestAsHex(password.getBytes());
        //构造条件
        QueryWrapper<User> Wrapper = new QueryWrapper<>();
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("password", hashedPassword);
        Wrapper.allEq(map);
        //根据条件查询用户信息
        User user = this.userMapper.selectOne(Wrapper);
        //根据查询结果进行处理
        if (user == null) throw new LoginCredentialsException();
        if (user.getEnabled() == 0) throw new UserDisabledException();
        //生产登录日志
        LoginLog loginLog = new LoginLog();
        HttpServletRequest request = SpringUtils.getRequest();
        String ip = IpUtils.getIpAddr(request);
        String location = AddressUtils.getRealAddressByIP(ip);
        loginLog.setLoginIp(ip);
        loginLog.setLoginLocation(location);
        loginLog.setLoginTime(LocalDateTime.now());
        loginLog.setCreateBy(user.getUsername());
        loginLog.setUsername(user.getUsername());
        this.loginLogMapper.insert(loginLog);
        //返回结果
        return user;

    }

    @Override
    public List<String> getUserIdentity(String username) {
        return this.userMapper.getUserIdentity(username);
    }

    @Override
    public List<Map> getUserInfo() {
        return this.userMapper.getUserInfo();
    }

    @Override
    public List<Map> getAll() {
        return this.userMapper.getAll();
    }

    @Override
    public boolean updateById(User entity) {
        //1.修改用户密码
        if (entity.getPassword() != null) {
            //密码加密
            String hashedPassword = DigestUtils.md5DigestAsHex(entity.getPassword().getBytes());
            entity.setPassword(hashedPassword);
            return super.updateById(entity);
        }
        //2.启用、禁用账号
        if (entity.getEnabled() != null) {
            Integer enabled = entity.getEnabled();
            //如果启用则禁用
            if (enabled == 1) entity.setEnabled(0);
            //如果禁用则启用
            if (enabled == 0) entity.setEnabled(1);
            return super.updateById(entity);
        }
        return true;
    }

    @Override
    public List<Map<String, Object>> getUserRoleById(int userId) {
        return this.userMapper.getUserRoleById(userId);
    }
}




