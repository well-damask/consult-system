package com.example.consultapi.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.CustBackup;
import com.example.consultapi.mapper.CustBackupMapper;
import com.example.consultapi.service.CustBackupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author helan
 * @description 针对表【cust_backup(客户基本信息备份表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class CustBackupServiceImpl extends ServiceImpl<CustBackupMapper, CustBackup>
        implements CustBackupService {
    @Resource
    CustBackupMapper custBackupMapper;

    @Override
    public boolean save(CustBackup entity) {
        //备份之前的检查
        String weChat = entity.getWx().trim();
        String phone = entity.getPhone().trim();
        //数据验证（手机号码和微信号必须填一个）
        if (StrUtil.isBlank(weChat) && StrUtil.isBlank(phone)) throw new RuntimeException("电话号码或微信必须填一个");
        return super.save(entity);
    }
}




