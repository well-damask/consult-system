package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.ProjectCategory;

import java.util.List;

/**
 * @author helan
 * @description 针对表【project_category(项目类别表)】的数据库操作Service
 * @createDate 2023-07-17 15:58:26
 */
public interface ProjectCategoryService extends IService<ProjectCategory> {
    /**
     * 获取项目类别表中的信息
     *
     * @return 返回项目类别集合
     */
    List<ProjectCategory> getProject();
}
