package com.example.consultapi.exception;

/**
 * 登录凭证错误异常（帐号或密码错误）
 */
public class LoginCredentialsException extends RuntimeException {
    public LoginCredentialsException() {
    }

    public LoginCredentialsException(String message) {
        super(message);
    }

    public LoginCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoginCredentialsException(Throwable cause) {
        super(cause);
    }

    public LoginCredentialsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
