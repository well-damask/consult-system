package com.example.consultapi.dto;

import lombok.Data;

import java.util.List;

@Data
public class AssignModel {
    private List<Custs> selectedCusts;
    private List<String> selectedUsernames;

    @Data
    public static class Custs {
        private Integer custId;
        private String prevOwner;
    }
}
