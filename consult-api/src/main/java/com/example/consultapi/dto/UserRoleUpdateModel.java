package com.example.consultapi.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserRoleUpdateModel {
    private Integer userId;
    private List<Integer> newRoleId;
    private List<Integer> oldRoleId;
}
