package com.example.consultapi;

import com.example.consultapi.domain.Cust;
import com.example.consultapi.domain.TransferLog;
import com.example.consultapi.mapper.CustMapper;
import com.example.consultapi.mapper.TransferLogMapper;
import com.example.consultapi.service.CustService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component
public class AutoTransfer {
    @Resource
    CustMapper custMapper;
    @Resource
    CustService custService;
    @Resource
    TransferLogMapper transferLogMapper;

    //每天凌晨0点自动转让
    @Scheduled(cron = "0 0 0 * * * * ")
    public void doAutoTransfer() {
        //查询数据库超过6个月没有联系的客户
        List<Map<String, Object>> list = this.custMapper.getCustIdBySixMonthNoConnect();
        for (Map<String, Object> map : list) {
            int custId = (int) map.get("id");
            //将他们的负责人设置为null,自动转让标志为1
            this.custService.lambdaUpdate().set(Cust::getAutoTransferred, 1).set(Cust::getCounselorUsername, null).update();
            //生成自动转让日志
            TransferLog transferLog=new TransferLog();
            transferLog.setCustId(custId);
            this.transferLogMapper.insert(transferLog);
        }

    }
}
