package com.example.consultapi.utils;

import cn.hutool.core.util.StrUtil;
import com.example.consultapi.dto.AssignModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssignUtils {
    List<AssignModel.Custs> custs;
    List<String> usernames;
    int pointer1 = 0; // 正常应该接受分配的咨询师的下标（轮到谁了？）
    int pointer2 = 0; // 原负责人和pointer1发生冲突时，接受调剂的咨询师的下标（上次调剂给了谁？）
    int index = 0; // 实际接受分配的咨询师的下标

    //生成构造方法给custs/usernames赋值
    public AssignUtils(List<AssignModel.Custs> custs, List<String> usernames) {
        this.custs = custs;
        this.usernames = usernames;
    }

    /**
     * 生成分配计划（在内存在构建好custId -> username的映射）
     *
     * @return
     */
    public Map<Integer, String> getAssignPlan() {
        //准备一个hashmap用于存储被分配的客户id对应的咨询老师账号
        Map<Integer, String> map = new HashMap<>();
        //循环遍历生成
        for (AssignModel.Custs cust : this.custs) {
            //拿到客户开始分配(实际接受分配的咨询师的下标)
            this.tryAssign(cust);
            //通过实际分配老师的下标拿到账号
            String username = this.usernames.get(index);
            map.put(cust.getCustId(), username);
        }
        return map;
    }

    /**
     * 拿到一个客户的id，负责人账号，开始分配
     *
     * @param cust 装有客户id，负责人账号的实体类（Custs）
     */
    public void tryAssign(AssignModel.Custs cust) {
        //拿到该分配客户原负责人
        String prevOwner = cust.getPrevOwner();
        //如果原负责人和应该接受分配的咨询师（pointer1）相同，则分配给pointer2(调剂)，否则就直接分配给point1
        if (StrUtil.equals(prevOwner, this.usernames.get(pointer1))) {
            //调剂
            this.pointer2Next();
            //实际接受分配的咨询师的下标
            index = pointer2;
        } else {
            index = pointer1;
            //下次该轮到
            pointer1Next();
        }
    }

    /**
     * 用于调剂
     */
    public void pointer2Next() {
        this.pointer2++;
        if (this.pointer2 == this.usernames.size()) this.pointer2 = 0;
        if (this.pointer2 == this.pointer1) this.pointer2Next();
    }

    /**
     * 用于计算应该轮到谁
     */
    public void pointer1Next() {
        this.pointer1++;
        if (this.pointer1 == this.usernames.size()) this.pointer1 = 0;
    }
}
