package com.example.consultapi.utils;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Slf4j
@Component
public class AutoFillUtils implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createTime", LocalDateTime::now, LocalDateTime.class);
        //获取请求中的当前用户
        HttpServletRequest request = SpringUtils.getRequest();
        this.strictInsertFill(metaObject, "createBy", () -> request.getHeader("Authorization"), String.class);
        this.strictInsertFill(metaObject, "counselorUsername", () -> request.getHeader("Authorization"), String.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime::now, LocalDateTime.class);
        HttpServletRequest request = SpringUtils.getRequest();
        this.strictUpdateFill(metaObject, "updateBy", () -> request.getHeader("Authorization"), String.class);
    }
}
