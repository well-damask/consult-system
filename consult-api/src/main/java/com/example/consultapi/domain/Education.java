package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 学历表
 *
 * @TableName education
 */
@TableName(value = "education")
@Data
public class Education extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 学历
     */
    @NotBlank(message = "学历不能为空")
    private String eduName;
}