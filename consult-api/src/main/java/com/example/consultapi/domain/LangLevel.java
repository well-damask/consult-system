package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 语言等级表
 *
 * @TableName lang_level
 */
@TableName(value = "lang_level")
@Data
public class LangLevel extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 语言等级
     */
    @NotBlank(message = "语言等级不能为空")
    private String levelName;
}