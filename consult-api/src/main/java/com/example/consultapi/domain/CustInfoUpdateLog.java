package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 客户基本信息（姓名、手机号码、微信号等）变更日志表
 *
 * @TableName cust_info_update_log
 */
@TableName(value = "cust_info_update_log")
@Data
public class CustInfoUpdateLog extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * FK，基本信息被变更的客户ID
     */
    private Integer custId;
}