package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 登录日志表
 *
 * @TableName login_log
 */
@TableName(value = "login_log")
@Data
public class LoginLog extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 登录时间
     */
    private LocalDateTime loginTime;
    /**
     * 登录IP
     */
    private String loginIp;
    /**
     * 登录地区
     */
    private String loginLocation;
    /**
     * 登录者帐号
     */
    private String username;
}