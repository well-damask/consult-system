package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 客户分配日志表
 *
 * @TableName assign_log
 */
@TableName(value = "assign_log")
@Data
public class AssignLog extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * FK，受让方（接过这个客户的咨询老师的帐号）
     */
    private String counselorUsername;
    /**
     * FK，被分配的客户ID
     */
    private Integer custId;
}