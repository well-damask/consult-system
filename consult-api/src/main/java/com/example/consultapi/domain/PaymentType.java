package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 付款方式表
 *
 * @TableName payment_type
 */
@TableName(value = "payment_type")
@Data
public class PaymentType extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 付款方式
     */
    @NotBlank(message = "付款方式不能为空")
    private String typeName;
}