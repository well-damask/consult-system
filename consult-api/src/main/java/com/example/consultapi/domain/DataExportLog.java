package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据导出日志表
 *
 * @TableName data_export_log
 */
@TableName(value = "data_export_log")
@Data
public class DataExportLog extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 被导出的excel文件名称
     */
    private String excelFileName;
}