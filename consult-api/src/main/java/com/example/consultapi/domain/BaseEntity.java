package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;

// 在编译这个BaseEntity.java时，IDEA检测到这个类的头顶上有@Getter @Setter注解，所以它采用lombok插件提供的特殊编译器
// 自动在BaseEntity.class字节码文件中添加与属性对应的Getter与Setter方法！
@Data
public class BaseEntity {
    /**
     * PK_ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 创建时间
     */
    //告诉mybatis-plus在插入一个新实体时，帮我添加这一属性
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 创建人（完成此次分配的主管的帐号）
     */
    //告诉mybatis-plus在插入一个新实体时，帮我添加这一属性
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 最近更新时间
     */
    //告诉mybatis-plus在更新一个实体时，帮我添加这一属性
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 最近更新人
     */
    //告诉mybatis-plus在更新一个实体时，帮我添加这一属性
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=删除
     */
    private Integer delFlag;

}
