package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 项目类别表
 *
 * @TableName project_category
 */
@TableName(value = "project_category")
@Data
public class ProjectCategory extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 项目类别名称
     */
    @NotBlank(message = "项目类别不能为空")
    private String categoryName;
}