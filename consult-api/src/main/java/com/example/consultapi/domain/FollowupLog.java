package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 跟进日志表
 *
 * @TableName followup_log
 */
@TableName(value = "followup_log")
@Data
public class FollowupLog extends BaseEntity implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * FK，被跟进的客户ID
     */
    private Integer custId;
    /**
     * 跟进情况
     */
    private String description;
}