package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 客户状态表
 *
 * @TableName cust_status
 */
@TableName(value = "cust_status")
@Data
public class CustStatus extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 状态名称
     */
    @NotBlank(message = "客户状态不能为空")
    private String statusName;
}