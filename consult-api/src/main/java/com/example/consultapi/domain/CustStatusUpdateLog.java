package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 客户状态变更日志表
 *
 * @TableName cust_status_update_log
 */
@TableName(value = "cust_status_update_log")
@Data
public class CustStatusUpdateLog extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * FK，状态被变更的客户ID
     */
    private Integer custId;
    /**
     * 变更前的状态
     */
    private Integer oldStatusId;
    /**
     * 变更后的状态
     */
    private Integer newStatusId;
}