package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 客户转让日志表
 *
 * @TableName transfer_log
 */
@TableName(value = "transfer_log")
@Data
public class TransferLog extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * FK，被转让的客户ID
     */
    private Integer custId;
    /**
     * 转让说明
     */
    private String description;
}