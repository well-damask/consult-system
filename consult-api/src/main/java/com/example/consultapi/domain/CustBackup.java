package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 客户基本信息备份表
 *
 * @TableName cust_backup
 */
@TableName(value = "cust_backup")
@Data
public class CustBackup extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 备份时间
     */
    private LocalDateTime backupTime;
    /**
     * 客户ID
     */
    private Integer custId;
    /**
     * 客户的姓
     */
    private String lastName;
    /**
     * 客户的名
     */
    private String firstName;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 微信号
     */
    private String wx;
    /**
     * 性别：男/女
     */
    private String gender;
    /**
     * FK，学历ID
     */
    private Integer eduId;
    /**
     * 学校
     */
    private String university;
    /**
     * 专业
     */
    private String major;
    /**
     * FK，语言等级ID
     */
    private Integer langLevelId;
    /**
     * FK，项目类别ID
     */
    private Integer projectCategoryId;
    /**
     * FK，客户来源ID
     */
    private Integer sourceId;
    /**
     * FK，客户状态ID
     */
    private Integer statusId;
    /**
     * FK，负责人（咨询老师）的用户ID
     */
    private Integer counselorUserId;
    /**
     * 是否是被某咨询老师转让出来的客户：0=不是，1=是
     */
    private Integer manualTransferred;
    /**
     * 是否因超过6个月未联系被系统自动转让出来：0=不是，1=是
     */
    private Integer autoTransferred;
    /**
     * 成交日期（报名日期）
     */
    private LocalDate dealDate;
}