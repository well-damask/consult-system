package com.example.consultapi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // 标识这是一个SpringBoot应用
@MapperScan("com.example.consultapi.mapper") // 通知SpringBoot框架：请你扫描这个包下的mapper接口，这样我们就不用在每个mapper接口头顶上写@Mapper
public class ConsultApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsultApiApplication.class, args);
    }


}
