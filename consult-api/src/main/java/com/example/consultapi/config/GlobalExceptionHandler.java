package com.example.consultapi.config;

import com.example.consultapi.exception.LoginCredentialsException;
import com.example.consultapi.exception.UserDisabledException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 */
@RestControllerAdvice // 你的那些RestController发生了异常的话，就自动通知这个类来处理。
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception e) {
        e.printStackTrace();
        if (e instanceof LoginCredentialsException) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("帐号或密码错误");
        } else if (e instanceof UserDisabledException) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("该帐号已被禁用");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
