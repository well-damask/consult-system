import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from '../views/LoginView.vue'

Vue.use(VueRouter)

// 配置路由映射表
const routes = [
    {
        path: '/',
        name: 'login',
        component: LoginView
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('../views/HomeView.vue'), // HomeView中有二级坑：<router-view>，用来填充这个二级坑的组件就配置为二级路由：children
        children: [
            {
                path: '/cust-source',
                name: 'CustSource',
                component: () => import('../views/basedatamanager/CustSourceView.vue')
            },
            {
                path: '/cust-status',
                name: 'CustStatus',
                component: () => import('../views/basedatamanager/CustStatusView.vue')
            },
            {
                path: '/payment_type',
                name: 'PaymentType',
                component: () => import('../views/basedatamanager/PaymentTypeView.vue')
            },
            {
                path: '/education',
                name: 'Education',
                component: () => import('../views/basedatamanager/EducationView.vue')
            },
            {
                path: '/language_level',
                name: 'LanguageLevel',
                component: () => import('../views/basedatamanager/LanguageLevelView.vue')
            },
            {
                path: '/project_category',
                name: 'ProjectCategory',
                component: () => import('../views/basedatamanager/ProjectCategoryView.vue')
            },
            {
                path: '/custInfoReport',
                name: 'custInfoReport',
                component: () => import('../views/custmanager/CustInfoReportView.vue')
            },
            {
                path: '/duplicateCheck',
                name: 'duplicateCheck',
                component: () => import('../views/custmanager/DuplicateCheckView.vue')
            },
            {
                path: '/custQuery',
                name: 'custQuery',
                component: () => import('../views/custmanager/CustQueryView.vue')
            },
            {
                path: '/custTransfer',
                name: 'custTransfer',
                component: () => import('../views/custmanager/CustAssiginView.vue')
            },
            {
                path: '/assignLog',
                name: 'assignLog',
                component: () => import('../views/logmanager/AssignLogView.vue')
            },

            {
                path: '/custInfoUpdateLog',
                name: 'custInfoUpdateLog',
                component: () => import('../views/logmanager/CustInfoUpdateLogView.vue')
            },

            {
                path: '/custStatusUpdateLog',
                name: 'custStatusUpdateLog',
                component: () => import('../views/logmanager/CustStatusUpdateLogView.vue')
            },

            {
                path: '/dataExportLog',
                name: 'dataExportLog',
                component: () => import('../views/logmanager/DataExportLogView.vue')
            },
            {
                path: '/followupLog',
                name: 'followupLog',
                component: () => import('../views/logmanager/FollowupLogView.vue')
            },
            {
                path: '/loginLog',
                name: 'loginLog',
                component: () => import('../views/logmanager/LoginLogView.vue')
            },
            {
                path: '/transferLog',
                name: 'transferLog',
                component: () => import('../views/logmanager/TransferLogView.vue')
            },
            {
                path: '/userList',
                name: 'userList',
                component: () => import('../views/systemmanager/UserListView.vue')
            },
        ]
    },
]

const router = new VueRouter({
    routes
})

export default router
