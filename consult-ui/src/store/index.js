import Vue from 'vue'
import Vuex from 'vuex'
import Http from "@/util/http";


Vue.use(Vuex)
const defaultState = {
    currentUser: {},
    custSource: [],
    custStatus: [],
    paymentType: [],
    languageLevel: [],
    education: [],
    projectCategory: [],
}

/**
 * 将全局状态存入会话存储
 * @param state 全局状态
 */
function presistent(state) {
    window.sessionStorage.setItem("state", JSON.stringify(state))
}

/**
 * 从会话存储中取出全局状态
 * @returns 全局状态state
 */
function loadState() {
    try {
        var stateJson = window.sessionStorage.getItem("state")
        if (stateJson) {
            var state = JSON.parse(stateJson)
            return state
        }
    } catch (e) {
        console.log(e)
    }
    return defaultState
}

const store = new Vuex.Store({
    // 状态
    state: loadState(),
    // 相当于基于state的计算属性
    getters: {},
    // 变更状态的方法
    mutations: {
        setCurrentUser(state, payload) {
            state.currentUser = payload;
            presistent(state)
        },
        setCustSource(state, payload) {
            state.custSource = payload;
            presistent(state)
        },
        setCustStatus(state, payload) {
            state.custStatus = payload;
            presistent(state)
        },
        setPaymentType(state, payload) {
            state.paymentType = payload;
            presistent(state)
        },
        setLanguageLevel(state, payload) {
            state.languageLevel = payload;
            presistent(state)
        },
        setEducation(state, payload) {
            state.education = payload;
            presistent(state)
        },
        setProjectCategory(state, payload) {
            state.projectCategory = payload;
            presistent(state)
        },


    },
    // 与后端API通信、调用mutations变更状态
    actions: {
        loadCustSource({commit}) {
            Http.get("/source").then(
                res => commit("setCustSource", res)
            )
        },
        loadCustStatus({commit}) {
            Http.get("/status").then(
                res => commit("setCustStatus", res)
            )
        },
        loadPaymentType({commit}) {
            Http.get("/payment").then(
                res => commit("setPaymentType", res)
            )
        },
        loadLanguageLevel({commit}) {
            Http.get("/level").then(
                res => commit("setLanguageLevel", res)
            )
        },
        loadEducation({commit}) {
            Http.get("/edu").then(
                res => commit("setEducation", res)
            )
        },
        loadProjectCategory({commit}) {
            Http.get("/pro").then(
                res => commit("setProjectCategory", res)
            )
        },


    },
    // 如果你需要集中管理的状态太多太复杂，可以把这个文件拆分成多个module，在这里来统一集成进来
    modules: {}
});


export default store;