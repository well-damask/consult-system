import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 全局引入ElementUI库，这样我们可以直接在其它的.vue文件中使用各种<el-xxx>组件
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 导入 ./store/index.js 中定义的store对象
import store from './store'

Vue.use(ElementUI)

Vue.config.productionTip = false

// 根组件：Root
new Vue({
    router,
    store, // 把这个store对象挂载到根组件上，这样呢在整个组件树中都可以访问到store

    // 渲染子组件：App
    render: h => h(App)
}).$mount('#app')
