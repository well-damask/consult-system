import axios from "axios";
import store from "@/store";
// 自定义axios实例，复用baseURL
const http = axios.create({
    baseURL: "http://localhost:8080"
})

// 添加请求拦截器
http.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    //在发送请求前，添加一个头信息（Authorization:当前用户）
    try {
        if (config.url !== "/login") {
            const currentUser = store.state.currentUser.username;
            config.headers["Authorization"] = currentUser
        }
    } catch (e) {
        console.log(e)
    }
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
http.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    var data = response.data;
    return data;
}, function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
});

export default http

